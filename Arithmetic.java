public class Arithmetic {

    public static void main(String[] arg) {
        int sum = add(Integer.parseInt(arg[0]), Integer.parseInt(arg[1]));
        int diff = subtract(Integer.parseInt(arg[0]), Integer.parseInt(arg[1]));

        System.out.println("Sum of numbers is " + sum);
        System.out.println("Difference of numbers is " + diff);
    }

    public static int add(int i, int j) {
        return i + j;
    }

    public static int subtract(int i, int j) {
        return i - j;
    }

    public static int mul(int i, int j) {
        int mul = 0;
        if(i!=0 && j!=0){
            mul =  i * j;
        }
        return mul;
    }

    public static int div(int i, int j) {
        int div = 0;
        if(j!=0){
            div = i / j;
        }else{
            throw new RuntimeException("Divide by zero cannot be done");
        }
        return div;
    }

}
